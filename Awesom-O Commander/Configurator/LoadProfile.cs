﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Core;

namespace Awesom_O_Commander
{
    public partial class LoadProfile : Form
    {
        List<Profile> profs;

        public LoadProfile()
        {
            InitializeComponent();
        }

        private void LoadProfile_Load(object sender, EventArgs e)
        {
            profs = BotSettings.GetSettings().Profiles;
            foreach (Profile prof in profs)
            {
                ListViewItem item = new ListViewItem(new[] {profs.IndexOf(prof, 0).ToString(), prof.DiabloAccount, prof.ProfileName});
                listView1.Items.Add(item);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection items = listView1.SelectedItems;
            if (items.Count == 1)
            {
                new Config(profs[Convert.ToInt32(items[0].SubItems[0].Text)]);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
