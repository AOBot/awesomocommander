﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Core
{
    [XmlRoot("BotSettings")]
    public class BotSetting
    {
        public int MaxGameTime;
        public int MinGameTime;
        public int MaxGamesAnHour;
        public int BanWaitTime;
        public int CreateGameDelay;
        public int JoinGameDelay;
        public int StartRunDelay;
        public int LoginTooLongDelay;
        public int GameJoinTooLongDelay;
        public int CdKeyInUseDelay;
        public int CantConnectDelay;
        public List<Profile> Profiles;

        public BotSetting() { }
    }

    [XmlRoot("Profile")]
    public class Profile
    {
        public string ProfileName;
        public string DiabloPath;
        public string DiabloOwnerName;
        public string DiabloAccount;
        public string DiabloPassword;
        public string Channel;
        public string GameName;
        public string GamePassword;
        public bool HasGamePass;
        public string FirstJoinChannelMessage;
        public string NextGameMessage;
        public string TooShortMessage;
        public int DiabloCharPosition;
        public int GameDifficulty;
        public int KeyChange;
        public bool HideDiabloWindow;
        public bool Follower;
        public bool Cobaaler;
        public bool Joiner;
        public bool AutoStartRun;
        public string MasterAccount;
        public string MasterCharacter;
        public string FollowGamePassword;
        public List<string> ClassicKeys;
        public List<string> ExpansionKeys;
        public bool AutoMule;  //nathan's mule
        public string MuleAccount; //nathan's mule
        public string MulePassword; //nathan's mule
        public string MuleCharNamePrefix; //nathan's mule
        public bool NowMuling; //nathan's mule
        public int MuleCharPos; //nathan's mule
        public string MuleGameName;
        public string MuleGamePassword;
        public int GameDifficultySave;
        public bool MakeMuleGame;
        public string FollowGameName;

        public Profile() { }
    }

    [XmlRoot("AoSettingStruct")]
    public class AoSettingStruct
    {
        public string AOPath;
        public string DiabloFolder;
        public string AoFlags;
        public string Realm;
        public string RunOnBanExePath;
        public bool Expert;
        public bool Stealth;
        public bool AoNoStart;
        public int ClickDelay;
        public int KeyDelay;
        public int StartDelay;
        public string ControllerIp;
        public bool ExpertIp;
        public bool HideCore;
        public bool IpChangerUse;
        public bool RunOnBan;

        public AoSettingStruct() { }
    }

    [XmlRoot("IpChangerStruct")]
    public class IpChangerStruct
    {
        public string ip;
        public string login;
        public string disconnect;
        public string connect;
        public string username;
        public string password;
        public string preAuthUrl;
        public string challengeUrl;
        public string challengeSearch;
        public bool post;
        public bool postDisconnect;
        public bool postReconnect;
        public bool preAuth;
        public bool challenge;
        public bool autoReconnect;

        public IpChangerStruct() { }
    }

    public static class BotSettings
    {
        public static string xmlpath = Application.StartupPath + "\\AoProfiles.xml";

        public static void WriteSettings(BotSetting config)
        {
            XmlSer.Serialize(xmlpath, typeof(BotSetting), config);
        }

        public static BotSetting GetSettings()
        {
            return (BotSetting)XmlSer.DeSerialize(xmlpath, typeof(BotSetting));
        }
    }

    public static class AoSettings
    {
        public static string xmlpath = Application.StartupPath + "\\AoCore.xml";

        public static void WriteSettings(AoSettingStruct config)
        {
            XmlSer.Serialize(xmlpath, typeof(AoSettingStruct), config);
        }

        public static AoSettingStruct GetSettings()
        {
            return (AoSettingStruct)XmlSer.DeSerialize(xmlpath, typeof(AoSettingStruct));
        }
    }

    public static class IpChanger
    {
        public static string xmlpath = Application.StartupPath + "\\IPChanger.xml";

        public static void WriteSettings(IpChangerStruct config)
        {
            XmlSer.Serialize(xmlpath, typeof(IpChangerStruct), config);
        }

        public static IpChangerStruct GetSettings()
        {
            return (IpChangerStruct)XmlSer.DeSerialize(xmlpath, typeof(IpChangerStruct));
        }

    }
}
