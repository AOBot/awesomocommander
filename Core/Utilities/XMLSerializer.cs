﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Core
{
    public static class XmlSer
    {
        public static void Serialize(string filepath, Type type, object obj)
        {
            try
            {
                using (TextWriter w = new StreamWriter(filepath))
                {
                    XmlSerializer s = new XmlSerializer(type);
                    s.Serialize(w, obj);
                    w.Close();
                }
            }
            catch (Exception) {  }
        }

        public static object DeSerialize(string filepath, Type type)
        {
            try
            {
                object obj = null;
                using (TextReader r = new StreamReader(filepath))
                {
                    XmlSerializer s = new XmlSerializer(type);
                    obj = s.Deserialize(r);
                    r.Close();
                }
                return obj;
            }
            catch (Exception) { return null; }
        }

    }
}
