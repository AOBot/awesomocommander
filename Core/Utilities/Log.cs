﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Core
{
    public class Log
    {
        static string LogPath;
        static string LogName;

        static Log()
        {
            LogPath = Application.StartupPath + "//logs";
            if (!Directory.Exists(LogPath)) { Directory.CreateDirectory(LogPath); }
            LogName = "//" + String.Format("{0:M-d-yy_H-m-s}", DateTime.Now) + ".log";
            if (File.Exists(LogPath + LogName)) { File.Delete(LogPath + LogName); }
            Divider(Application.ProductName + " v" + Application.ProductVersion);
        }

        private static void Write(string txt)
        {
            try
            {
                File.AppendAllText(LogPath + LogName, txt + Environment.NewLine);
            }
            catch { }
        }

        private static string Time()
        {
            return String.Format("{0:MM/dd/yyyy HH:mm:ss}", DateTime.Now);
        }

        public static void Debug(string txt)
        {
            Write(Time() + " -- Debug: " + txt);
        }

        public static void Info(string txt)
        {
            Write(Time() + " -- Info:  " + txt);
        }

        public static void Error(string txt)
        {
            Write(Time() + " -- Error: " + txt);
        }

        public static void Divider()
        {
            Write("======================================================================================"); //86 chars
        }

        public static void Divider(string txt)
        {
            txt = " " + txt.Trim() + " ";
            //
            int Half = (86 - txt.Length) / 2;
            string Begin = "";
            string End = "";
            for (int i = 1; i <= Half; i++) { Begin += "="; }
            End = Begin;
            if (((Half * 2) + txt.Length) > 86) { End.Remove(Half - 1); }
            else if (((Half * 2) + txt.Length) < 86) { End += "="; }
            //
            Divider();
            Write(Begin + txt + End);
            Divider(); ;
        }
    }
}