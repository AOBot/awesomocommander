﻿
namespace Core
{
    public class BotStatistic
    {
        private float gamecount = 0;
        private float gamecountsuccess = 0;
        private float ingametime = 0;
        private int bans = 0;
        private int chickens = 0;
        private int deaths = 0;
        private int gameExp = 0;
        private int totalExp = 0;

        public int Games { get { return (int)gamecount; } }

        public int AvgGameExp 
        { 
            get 
            {
                if (gamecountsuccess > 0)
                    return (int)(totalExp / gamecountsuccess);
                else
                    return gameExp;
            } 
        }

        public int TotalExpGained { get { return totalExp; } }

        public float SuccessRate
        {
            get 
            { 
                if (gamecount > 0 && gamecountsuccess > 0)
                    return (((gamecountsuccess) / gamecount) * 100);
                return 0;
            }
        }

        public int AverageGameTime
        {
            get
            {
                if (gamecountsuccess > 0 && ingametime > 0)
                    return (int)((ingametime / 1000) / gamecountsuccess);
                return 0;
            }
        }

        public int Bans { get { return bans; } }

        public int Chickens { get { return chickens; } }

        public int Deaths { get { return deaths; } }

        public void IncreaseGameCount() { gamecount++; }

        public void IncreaseGameCountSuccess() { gamecountsuccess++; }

        public void IncreaseBans() { bans++; }

        public void IncreaseChickens() { chickens++; }

        public void IncreaseDeaths() { deaths++; }

        public void AddInGameTime(long time) { ingametime += time; }

        public void AddGameExperience(int exp) { totalExp += exp; }       
    }
}
