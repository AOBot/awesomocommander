﻿using System.Collections.Generic;

namespace Core
{
    internal class CdKeyManager
    {
        private List<string> ClassicKeys = new List<string>();
        private List<string> ExpansionKeys = new List<string>();
        private Queue<string> ClassicKeysQueue = new Queue<string>();
        private Queue<string> ExpansionKeysQueue = new Queue<string>();

        public CdKeyManager(List<string> ClassicKeys, List<string> ExpansionKeys)
        {
            this.ClassicKeys = ClassicKeys;
            this.ExpansionKeys = ExpansionKeys;
            this.ClassicKeysQueue = new Queue<string>(ClassicKeys.ToArray());
            this.ExpansionKeysQueue = new Queue<string>(ExpansionKeys.ToArray());
        }

        public string GetNextClassicKey()
        {
            if (ClassicKeys.Count > 0)
            {
                if (ClassicKeysQueue.Count <= 0)
                    ClassicKeysQueue = new Queue<string>(ClassicKeys.ToArray());
                return ClassicKeysQueue.Dequeue();
            }

            return null;
        }

        public string GetNextExpansionKey()
        {
            if (ExpansionKeys.Count > 0)
            {
                if (ExpansionKeysQueue.Count <= 0)
                    ExpansionKeysQueue = new Queue<string>(ExpansionKeys.ToArray());
                return ExpansionKeysQueue.Dequeue();
            }

            return null;
        }
    }
}
