﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace Core
{
    public static class D2Launcher
    {
        private static string launcherpath = Application.StartupPath + "\\D2Launcher\\";

        private static bool CreateNewLauncherIni(string diapath)
        {
            try
            {
                using (StreamWriter s = new StreamWriter(launcherpath + "D2Launcher.ini", false))
                {
                    s.WriteLine("(Main)");
                    s.WriteLine("{");
                    s.WriteLine("D2Folder=\"" + diapath + "\";");
                    s.WriteLine("LoadcGuard=\"FALSE\";");
                    s.WriteLine("};");
                    s.Close();
                }
                return true;
            }
            catch (Exception e) { Log.Error("CreateNewLauncherIni(): " + e.Message); return false; }
        }

        public static bool StartD2Launcher(string diapath, string title)
        {
            try
            {
                if (CreateNewLauncherIni(diapath))
                {
                    Process D2LauncherProc = new Process();
                    D2LauncherProc.StartInfo.FileName = launcherpath + "D2Launcher.exe";
                    D2LauncherProc.StartInfo.Arguments = "-w -ns -skiptobnet -title " + "\"" + title + "\"";
                    D2LauncherProc.StartInfo.WorkingDirectory = launcherpath;
                    D2LauncherProc.EnableRaisingEvents = true;
                    bool Started = D2LauncherProc.Start();
                    return Started;
                }
                return false;
            }
            catch (Exception e) { Log.Error("StartD2Launcher(): " + e.Message); return false; }
        }
    }
}
