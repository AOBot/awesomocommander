﻿
namespace Core
{    
    internal static class AoRealmHelper
    {
        public static void PreventAct5Bug(string diapath)
        {
            Microsoft.Win32.RegistryKey DiabloKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Blizzard Entertainment\\Diablo II", true);
            string Installpath = (string)DiabloKey.GetValue("InstallPath");
            if (!string.IsNullOrEmpty(Installpath))
            {
                if (Installpath != diapath)
                {
                    DiabloKey.DeleteValue("InstallPath");
                    DiabloKey.SetValue("InstallPath", diapath);
                }
            }
        }

        public static void SetRealm()
        {            
            Microsoft.Win32.RegistryKey BattleNetKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\\Battle.net\\Configuration", true);
            string currentRealm;
            string[] data;
            data = (string[])BattleNetKey.GetValue("Diablo II Battle.net gateways");

            if (data != null)
            {
                if (((data.Length - 2) % 3) != 0)
                {
                    string[] gateways = new string[20];
                    gateways[0] = "1002";
                    gateways[1] = "05";
                    gateways[2] = "uswest.battle.net";
                    gateways[3] = "8";
                    gateways[4] = "U.S. West";
                    gateways[5] = "useast.battle.net";
                    gateways[6] = "6";
                    gateways[7] = " U.S. East";
                    gateways[8] = "asia.battle.net";
                    gateways[9] = "-9";
                    gateways[10] = "Asia";
                    gateways[11] = "europe.battle.net";
                    gateways[12] = "-1";
                    gateways[13] = "Europe";
                    gateways[14] = "classicbeta.battle.net";
                    gateways[15] = "8";
                    gateways[16] = "Classic Beta";
                    gateways[17] = "localhost";
                    gateways[18] = "0";
                    gateways[19] = "Private Realm";
                    BattleNetKey.SetValue("Diablo II Battle.net gateways", gateways);
                }
                else
                {
                    int count = 0;
                    int line = 0;
                    bool found = false;
                    currentRealm = data[1];

                    while (!found)
                    {
                        if (count <= data.Length)
                        {
                            if ((count + 1) >= data.Length)
                            {
                                break;
                            }
                            line++;
                            if (string.Compare(data[count + 1], "localhost") == 0
                                || string.Compare(data[count + 1], "127.0.0.1") == 0)
                            {
                                found = true;
                                break;
                            }
                        }
                        count++;
                    }

                    if (found)
                    {
                        string proxyRealm = ((((line - 2) / 3) + 1).ToString());
                        if (proxyRealm.Length == 1)
                        {
                            proxyRealm = "0" + proxyRealm;
                        }
                        if (currentRealm == proxyRealm)
                        {
                        }
                        else
                        {
                            data[1] = proxyRealm;
                            BattleNetKey.SetValue("Diablo II Battle.net gateways", data);
                        }
                    }
                    else
                    {
                        int length = data.Length;
                        line = 0;
                        string[] newRealmKey = new string[length + 3];
                        foreach (var s in data)
                        {
                            newRealmKey[line] = s;
                            line++;
                        }
                        newRealmKey[line] = "localhost";
                        newRealmKey[line + 1] = "0";
                        newRealmKey[line + 2] = "Private Realm";
                        BattleNetKey.SetValue("Diablo II Battle.net gateways", newRealmKey);
                        SetRealm();
                    }
                }
            }
        }
    }
}
